import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.css']
})
export class NewProductComponent implements OnInit {

  productFormGroup! : FormGroup

  constructor(private formBuilder : FormBuilder , public productService : ProductService) 
  {}
  ngOnInit(): void {
    this.productFormGroup = this.formBuilder.group({
      name : this.formBuilder.control(null , [Validators.required,Validators.minLength(4)]),
      price : this.formBuilder.control(null , [Validators.required,Validators.min(4)]),
      promotion : this.formBuilder.control(false , [Validators.required])
    })
  }

  handleAddProduct(){
    let product = this.productFormGroup.value;
    this.productService.addNewProduct(product).subscribe({
      next:(data)=>{
        this.productFormGroup.reset();
        alert('Products added succesfully');
      },error:(err) =>{
        console.log(err);
      }
    });
  }
  
}
