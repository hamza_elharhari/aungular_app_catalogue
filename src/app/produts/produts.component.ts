import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Product } from '../model/product.model';
import { AuthenticationService } from '../services/authentication.service';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-produts',
  templateUrl: './produts.component.html',
  styleUrls: ['./produts.component.css']
})
export class ProdutsComponent implements OnInit  {


  products!: Array<Product> ;
  errMessage!: string; 
  searchFormGroup! : FormGroup ;
  currentPage : number = 0 ;
  pageSize : number = 5 ;
  totalPages : number = 0;
  currentAction: string='all';

  constructor(private productService : ProductService , private formBuilder : FormBuilder ,
    public authService : AuthenticationService , private router : Router  ){}

  ngOnInit(): void {
    this.searchFormGroup = this.formBuilder.group(
      {
        keyword: this.formBuilder.control(null)
      }
    )
    this.handelGetPageProducts();
  }

  public handelGetPageProducts(){
    this.productService.getPageProducts(this.currentPage, this.pageSize ).subscribe(
      {
        next : (data)=> {
          this.products = data.products;
          this.totalPages = data.totalPages;
        },
        error : (err)=>{
          this.errMessage = err;
        }
      });
  }

  public handelGetProducts(){
    this.productService.getAllProducts().subscribe(
      {
        next : (data)=> {
          this.products = data;
        },
        error : (err)=>{
          this.errMessage = err;
        }
      });
  }

  public handelDeleteProduct(product : Product){
    let conf = confirm('etes vous sur de vouloir supprimer');
    if(conf == false) return ;
    this.productService.deleteProduct(product.id).subscribe({
      next: (data)=>{
        //this.handelGetProducts();
        let index= this.products.indexOf(product);
        this.products.splice(index,1);
      }
    });
  }

  public handelSetPromotion(product: Product){
    let promo = product.promotion
    this.productService.setPromotion(product.id).subscribe({
      next : (data)=> {
        product.promotion = !promo;
      },
      error : (err)=>{
        this.errMessage = err;
      }
    });
  }

  handleSearchProducts() {
    this.currentAction='search';
    //this.currentPage=0 ;
   let keyword = this.searchFormGroup.value.keyword;
   this.productService.searchProducts(keyword , this.currentPage, this.pageSize).subscribe({
    
    next : (data)=>{ 
      this.products = data.products;
      this.totalPages = data.totalPages;
    }
   })
  }
  goToPage(i : number){
    
    this.currentPage = i ;
    if(this.currentAction ==='all'){
      this.handelGetPageProducts();
    }else{
      console.log('tot');
      this.handleSearchProducts();
    }
    
  }

  handleNewProduct(){
    this.router.navigateByUrl("/admin/newProduct");
  }

  handelEditProduct(p: Product){
    this.router.navigateByUrl("/admin/editProduct/"+p.id);
  }
}
