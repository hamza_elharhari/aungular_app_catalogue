import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Product } from '../model/product.model';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit{

  productId! :string ;
  productFormGroup! : FormGroup
  product! : Product ;
  constructor(private formBuilder : FormBuilder , private route : ActivatedRoute , public productService : ProductService ) 
  {
    this.productId = this.route.snapshot.params['id'];

  }

  ngOnInit(): void {
    this.productService.getProduct(this.productId).subscribe({
      next:(product)=>{
        this.product = product ;
        this.productFormGroup = this.formBuilder.group({
          name : this.formBuilder.control(product.name , [Validators.required,Validators.minLength(4)]),
          price : this.formBuilder.control(product.price , [Validators.required,Validators.min(4)]),
          promotion : this.formBuilder.control(product.promotion , [Validators.required])
        });
      },error:(err) =>{
        console.log(err);
      }
    });
  }
  handleUpdateProduct(){
    let p = this.productFormGroup.value;
    p.id = this.product.id;
    this.productService.updateProduct(p).subscribe({
      next:(product) => {
        alert("your Prodect has been Updated");
      },
      error: err =>{
        console.log(err);
      }
    });
  }

}
