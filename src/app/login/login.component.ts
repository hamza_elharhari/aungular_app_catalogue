import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Route, Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{

userFormGroup! : FormGroup;
errMessage : any ;

constructor(private formBuilder : FormBuilder,
   private authService : AuthenticationService,
   private router : Router){}

  ngOnInit(): void {
    this.userFormGroup = this.formBuilder.group({
      username : this.formBuilder.control(""),
      password : this.formBuilder.control("")
    });
  }

  public handleLogin(){

    let username = this.userFormGroup.value.username;
    let password = this.userFormGroup.value.password;
    
    this.authService.login(username,password).subscribe({
      next : (appUser)=> {
          this.authService.authenticateUser(appUser).subscribe({
            next : ()=>{
              this.router.navigateByUrl("admin");
            }
          });
      },
      error : (err)=>{
        this.errMessage = err ;
      }
    });
    
  }
}
